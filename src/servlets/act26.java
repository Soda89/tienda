package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.Connection;

/**
 * Servlet implementation class act26
 */
@WebServlet("/act26")
public class act26 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Connection con = null;
    private String version = "";
    private String usuario = "";
    private String url = "";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public act26() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init(ServletConfig config) throws ServletException {
    	super.init(config);
    	// Se leen los parámetros de inicialización de Servlet y
    	// se convierten en atributos del contexto para compartirlos con
    	// cualquier servlet y JSP de la aplicación
    	ServletContext context = config.getServletContext();
    	context.setAttribute("URLBaseDeDatos",config.getInitParameter("URLBaseDeDatos"));
    	context.setAttribute("usuario", config.getInitParameter("usuario"));
    	context.setAttribute("version", config.getInitParameter("version"));
    	
    	// Se recuperan las variables de contexto de la aplicacion
    	usuario=(String)context.getAttribute("usuario");
    	url=(String)context.getAttribute("URLBaseDatos");
    	version = (String)context.getAttribute("version");
    	
    	try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/tienda", "root", "");
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    } 
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		out.println(version);
	}

}
