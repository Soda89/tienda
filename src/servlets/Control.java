package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelos.Carrito;
import modelos.Comentario;
import modelos.Producto;
import modelos.Usuario;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


/**
 * Servlet implementation class Control
 */
@WebServlet("/control")
public class Control extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Connection con = null;
	PreparedStatement sentencia;
	Statement sentenciaSQL;
	ArrayList<String> operacionesTexto;
	private String version = "";
    private String usuario = "";
    private String url = "";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Control() {
        super();
        operacionesTexto = new ArrayList<String>();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:mysql://localhost/tienda", "root", "");
			if (con!=null)
			{
				System.out.println("Conexion OK");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error al cargar e driver");
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Error en la conexion");
			
		} catch (InstantiationException e) {
			e.printStackTrace();
			
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String ocul = request.getParameter("oculto");
		System.out.println("Oculto: "+ocul);
		int oculto = Integer.parseInt(ocul);
		HttpSession sesion = request.getSession();
		sesion.setAttribute("operaciones", operacionesTexto);
		ArrayList<Producto> listaProductos = new ArrayList<Producto>();
		
		//variables producto
		String nombre = "", descripcion = "", imgUrl = "";
		int idProducto = 0, stock = 0;
		double precio = 0;
		
		//variables comentario
		int valoracion = 0, idUsuario = 0;
		String comentario = "";
		
		switch (oculto)
		{
		// pagina principal, listado de productos
		case 1: String sql = "select * from productos";
				
				// recoge el carrito de la sesion, si no existe lo crea
				Carrito carrito = (Carrito) sesion.getAttribute("carrito");
				if (carrito == null) sesion.setAttribute("carrito", carrito);
			try {
				sentenciaSQL = con.createStatement();
				ResultSet resultados = sentenciaSQL.executeQuery(sql); // recoge los productos
				while (resultados.next())
				{
					nombre = resultados.getString("nombre");
					descripcion = resultados.getString("descripcion");
					imgUrl = resultados.getString("imgUrl");
					idProducto = resultados.getInt("idProducto");
					stock = resultados.getInt("stock");
					precio = resultados.getDouble("precio");
		
					// crea un objeto de la clase Producto con los campos de la BD
					// los a�ade a un arrayList que los mostrar� en la vista
					Producto p = new Producto(idProducto, nombre, descripcion, precio, stock, imgUrl);
					listaProductos.add(p);
					sesion.setAttribute("listaProductos", listaProductos);
					
					// recoge los comentarios de cada producto
					sentencia = con.prepareStatement("select * from reviews where idProducto = ?");
					sentencia.setInt(1, idProducto);
					ResultSet cmnts = sentencia.executeQuery();
					while (cmnts.next())
					{
						idUsuario = cmnts.getInt("idUsuario");
						comentario = cmnts.getString("comentario");
						valoracion = cmnts.getInt("valoracion");
						
						sentencia = con.prepareStatement("select nombre from usuarios where idUsuario = ?");
						sentencia.setInt(1, idUsuario);
						ResultSet nombreRes = sentencia.executeQuery();
						Comentario comentarioTmp = null;
						while (nombreRes.next())
						{
							comentarioTmp = new Comentario(idProducto, nombreRes.getString("nombre"), comentario, valoracion);
						}
						
						// a�ade cada comentario al producto
						p.addComentario(comentarioTmp);
					}
				}
				
				request.getRequestDispatcher("tienda.jsp").forward(request,response);
				break;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		// registro de usuarios (logeo es el 7)
		case 2: 
				// recoge los parametros de la request
				nombre = request.getParameter("nombre");
				String password = request.getParameter("password");
				String direccion = request.getParameter("direccion");
				String poblacion = request.getParameter("poblacion");
				int cp = Integer.parseInt(request.getParameter("cp"));
				String pais = request.getParameter("pais");
				
				// prepara la sentencia INSERT para la base de datos
			try {
				sentencia = con.prepareStatement("INSERT INTO usuarios(nombre, password, direccion, poblacion, cp, pais) "
						+ "VALUES(?, ?, ?, ?, ?, ?)");
				sentencia.setString(1, nombre);
				sentencia.setString(2, password);
				sentencia.setString(3, direccion);
				sentencia.setString(4, poblacion);
				sentencia.setInt(5, cp);
				sentencia.setString(6, pais);
				sentencia.executeUpdate();
				
				// recoge el campo ID del nuevo usuario que la BD ha generado automaticamente
				// para crear un nuevo objeto de la clase Usuario
				sentencia = con.prepareStatement("SELECT * from usuarios WHERE nombre = ? AND password = ?");
				sentencia.setString(1, nombre);
				sentencia.setString(2, password);
				ResultSet resultados = sentencia.executeQuery();
				int id = 0;
				while(resultados.next())
				{
					id = resultados.getInt("idUsuario");
				}
				Usuario usuario = new Usuario(id, nombre, password, direccion, poblacion, cp, pais);
				
				sesion.setAttribute("usuario", usuario);
				request.getRequestDispatcher("perfil.jsp").forward(request,response);
				
				break;
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
		// carrito
		case 3: break;
		
		// vista individual del producto con sus respectivos comentarios
		case 4: 
				// recoge el ID del producto que quiere ver el usuario
				idProducto = Integer.parseInt(request.getParameter("idP"));
				sql = "select * from productos where idProducto = ?";
			
				try {
					sentencia = con.prepareStatement(sql);
					sentencia.setInt(1, idProducto);
					ResultSet resultados = sentencia.executeQuery();
					
					// obtener los datos del producto
					while (resultados.next())
					{
						nombre = resultados.getString("nombre");
						descripcion = resultados.getString("descripcion");
						imgUrl = resultados.getString("imgUrl");
						idProducto = resultados.getInt("idProducto");
						stock = resultados.getInt("stock");
						precio = resultados.getDouble("precio");
			
						Producto p = new Producto(idProducto, nombre, descripcion, precio, stock, imgUrl);
						
						//obtener los comentarios del producto
						sentencia = con.prepareStatement("select * from reviews where idProducto = ?");
						sentencia.setInt(1, idProducto);
						ResultSet comentarios = sentencia.executeQuery();
						while (comentarios.next())
						{
							
							comentario = comentarios.getString("comentario");
							valoracion = comentarios.getInt("valoracion");
							idUsuario = comentarios.getInt("idUsuario");
							
							// busco el nombre de cada usuario que ha comentado el producto,
							// de modo que lo muestre en lugar del idUsuario
							sentencia = con.prepareStatement("select nombre from usuarios where idUsuario = ?");
							sentencia.setInt(1, idUsuario);
							ResultSet usuarioRes = sentencia.executeQuery();
							String nombreUser = "";
							while (usuarioRes.next())
							{
								nombreUser = usuarioRes.getString("nombre");
							}
							
							Comentario objetoComentario = new Comentario(idProducto, nombreUser, comentario, valoracion);
							
							// a�ade el comentario 
							p.addComentario(objetoComentario);
						}
						
						// guarda el producto en la sesion para acceder a �l desde la vista
						sesion.setAttribute("producto", p);
					}
					
					request.getRequestDispatcher("producto.jsp").forward(request,response);
					break;
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//insertar nuevo comentario
				case 5:
					// recoger los datos de la request
					Usuario usuario = (Usuario) sesion.getAttribute("usuario");
					String texto = request.getParameter("comentario");
					valoracion = Integer.parseInt(request.getParameter("valoracion"));
					idProducto = Integer.parseInt(request.getParameter("producto"));
					String nombreUsuario = usuario.getNombre();
					idUsuario = Integer.parseInt(request.getParameter("usuario"));
					
					// crea un objeto de la clase Comentario y lo a�ade al modelo
					Comentario comentarioParaInsertar = new Comentario(idProducto, idUsuario, texto, valoracion);
					usuario.addComentario(comentarioParaInsertar);
					
					// guardamos el comentario en la BD usando los datos de �ste y del usuario
					try {
						sentencia = con.prepareStatement("INSERT INTO reviews(idProducto, idUsuario, comentario, valoracion)"
						+ " VALUES(?, ?, ?, ?)");
						sentencia.setInt(1, idProducto);
						sentencia.setInt(2, idUsuario);
						sentencia.setString(3, texto);
						sentencia.setInt(4, valoracion);
						
						sentencia.executeUpdate();
						
						// devuelvo el usuario con los comentarios actualizados a la sesion
						sesion.setAttribute("usuario", usuario);
						
						request.getRequestDispatcher("producto.jsp?oculto=4&idP=" + idProducto).forward(request, response);
						break;
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
				//eliminar comentario
				case 6:
						// recoger los datos de la request
						idUsuario = Integer.parseInt(request.getParameter("usuario"));
						idProducto = Integer.parseInt(request.getParameter("producto"));
						
						// obtengo el usuario de la sesion, para eliminar el comentario de sus datos
						usuario = (Usuario) sesion.getAttribute("usuario");
						
						try {
							// primero busco el comentario para eliminarlo del modelo de datos
							sentencia = con.prepareStatement("SELECT * from reviews WHERE idProducto = ? AND idUsuario = ?");
							sentencia.setInt(1, idUsuario);
							sentencia.setInt(2, idProducto);
							
							ResultSet comentariosParaEliminar = sentencia.executeQuery();
							
							while (comentariosParaEliminar.next())
							{
								idUsuario = comentariosParaEliminar.getInt("idUsuario");
								idProducto = comentariosParaEliminar.getInt("idProducto");
								comentario = comentariosParaEliminar.getString("comentario");
								valoracion = comentariosParaEliminar.getInt("valoracion");
								
								Comentario comEliminar = new Comentario(idProducto, idUsuario, comentario, valoracion);
								usuario.eliminarComentario(idProducto, idUsuario);
							}
							
							// elimino del comentario de la BD
							sentencia = con.prepareStatement("DELETE from reviews WHERE idProducto = ? AND idUsuario = ?");
							sentencia.setInt(1, idProducto);
							sentencia.setInt(2, idUsuario);
							sentencia.executeUpdate();
							
							// devuelvo el usuario a la sesion
							sesion.setAttribute("usuario", usuario);
							
						
							request.getRequestDispatcher("perfil.jsp").forward(request, response);
							
							break;
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						break;
				
				//loguear user
				case 7: 
						// recoge datos de la request
						nombre = request.getParameter("nombre");
						String pass = request.getParameter("password");
						
						// compruebo el usuario en la BD
						try {
							sentencia = con.prepareStatement("SELECT * from usuarios WHERE nombre = ? AND password = ?");
							sentencia.setString(1, nombre);
							sentencia.setString(2, pass);
							Usuario u = new Usuario();
							ResultSet usuarioBD = sentencia.executeQuery();
							while(usuarioBD.next())
							{
								idUsuario = usuarioBD.getInt("idUsuario");
								nombre = usuarioBD.getString("nombre");
								password = usuarioBD.getString("password");
								direccion = usuarioBD.getString("direccion");
								poblacion = usuarioBD.getString("poblacion");
								cp = usuarioBD.getInt("cp");
								pais = usuarioBD.getString("pais");
								
								// creo el usuario correspondiente
								u = new Usuario(idUsuario, nombre, password, direccion, poblacion, cp, pais);
							}
							
							// recojo los comentarios de este usuario
							sentencia = con.prepareStatement("SELECT * from reviews WHERE idUsuario = ?");
							sentencia.setInt(1, idUsuario);
							ResultSet comentarios = sentencia.executeQuery();
							while(comentarios.next())
							{
								idUsuario = comentarios.getInt("idUsuario");
								idProducto = comentarios.getInt("idProducto");
								texto = comentarios.getString("comentario");
								valoracion = comentarios.getInt("valoracion");
								
								Comentario com = new Comentario(idProducto, idUsuario, texto, valoracion);
								
								// a�ade el comentario a los datos del modelo de usuario
								u.addComentario(com);
							}
							
							// a�ado el usuario como atributo de la sesion
							sesion.setAttribute("usuario", u);

							request.getRequestDispatcher("index.html").forward(request, response);
							
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
		}
	}
}
